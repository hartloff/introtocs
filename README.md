# README #

* Content for Intro to CS course sites goes here
* Master will be pushed to the live site (http://www.cse.buffalo.edu/~hartloff/IntroCS/)
* This is public so anyone can watch the progress on develop and feature branches

To add new content create a branch off of develop and work on that branch. Once all your changes are complete, merge your branch with develop. If develop is stable, it can be merged with master to be published to the live site.

Branching Model: http://nvie.com/posts/a-successful-git-branching-model/