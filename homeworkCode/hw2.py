def rockPaperScissor(playerOneChoice, playerTwoChoice):
    # inputs are {"rock", "paper", "scissors"}
    return ""  # return the winner. Either "player1", "player2", or "draw" in the case of a tie


def computeCommission(saleTotal, margin):
    # margin is {"low", "mid", "high"}
    # saleTotal is an integer
    return 0


def getFamilyCar(annualIncome, numberOfChildren):
    # both inputs are integers
    # return one of these car variables based on the income and number of children
    compactCar = "compact car"
    sportsCar = "sports car"
    superCar = "super car"
    stationWagon = "station wagon"
    fullSizeSedan = "full-size sedan"
    luxurySedan = "luxury sedan"
    minivan = "minivan"
    fullSizeSUV = "full-size SUV"
    return ""


def isCommonWebAddressFormat(inputURL):
    # return True if inputURL starts with "http" and ends with ".com" ".net" or ".org" and returns False otherwise
    return False


def test():
    # You can edit this function to fully test your code. This function will not be graded

    print("The winner is: " + str(rockPaperScissor("rock", "paper")))
    print("The winner is: " + str(rockPaperScissor("scissors", "paper")))
    print("The winner is: " + str(rockPaperScissor("rock", "rock")))
    print()
    print("$200000 of low margin products generated " + str(computeCommission(200000, "low")) + " in commission")
    print("$60000 of mid margin products generated " + str(computeCommission(60000, "mid")) + " in commission")
    print("$80000 of high margin products generated " + str(computeCommission(80000, "high")) + " in commission")
    print()

    children = 2
    baseSalary = 20000
    totalSalary = baseSalary + computeCommission(200000, "low") + computeCommission(60000, "mid") + computeCommission(
            80000, "high")
    print("With total salary $" + str(totalSalary) + " and " + str(children) + " children the family drives a " + str(
            getFamilyCar(totalSalary, children)))
    print()

    # True results
    print("'https://www.google.com' is a common web address format: " + str(isCommonWebAddressFormat("https://www.google.com")))
    print("'https://en.wikipedia.org' is a common web address format: " + str(isCommonWebAddressFormat("https://en.wikipedia.org")))
    print("'https://www.python.org' is a common web address format: " + str(isCommonWebAddressFormat("https://www.python.org")))
    print("'http://stackoverflow.com' is a common web address format: " + str(isCommonWebAddressFormat("http://stackoverflow.com")))
    print("'http://www.w3schools.com' is a common web address format: " + str(isCommonWebAddressFormat("http://www.w3schools.com")))
    print("'http://www.speedtest.net' is a common web address format: " + str(isCommonWebAddressFormat("http://www.speedtest.net")))

    # False results
    print("'http://buffalo.edu' is a common web address format: " + str(isCommonWebAddressFormat("http://buffalo.edu")))
    print("'http://weather.gov' is a common web address format: " + str(isCommonWebAddressFormat("http://weather.gov")))
    print("'www.google.com' is a common web address format: " + str(isCommonWebAddressFormat("www.google.com")))
    print("'website' is a common web address format: " + str(isCommonWebAddressFormat("website")))


test()
