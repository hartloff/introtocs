import json
import urllib.request
import hw5Server

def floatCompare(number1, number2):
    return abs(number1-number2) < 0.01

def partOneReady():
    request = {"request": "getReviews"}
    response = hw5Server.handleRequest(request)
    reviews = json.loads(response)
    count = 0
    for movie in reviews.keys():
        count += 1
        int(reviews[movie][0])
        if not len(str(reviews[movie][1])) > 0:
            return False
    if count >= 3:
        return True
    else:
        return False


def partTwoReady():
    theRequest = "http://www-student.cse.buffalo.edu/~hartloff/cgi-bin/courseRoster.cgi"
    result = urllib.request.urlopen(theRequest)
    jsonResult = result.read().decode('utf-8')
    listResult = json.loads(jsonResult)

    for ubit in listResult:
        passed = True
        theRequest = "http://www-student.cse.buffalo.edu/~hartloff/cgi-bin/interface.cgi?request=reviewsByID&ID=" + ubit
        result = urllib.request.urlopen(theRequest)
        topMovies = json.loads(result.read().decode('utf-8'))

        request = {"request": "reviewsByID", "ID": ubit}
        response = json.loads(hw5Server.handleRequest(request))

        if len(topMovies) != len(response):
            passed = False
        else:
            for movie in topMovies:
                if movie not in response:
                    passed = False
                    break

        if not passed:
            print(
                "Should Return: " + str(topMovies) + "\nReturned: " + str(response) + "\nFor Request: " + str(request))
            theRequest = "http://www-student.cse.buffalo.edu/~" + ubit + "/cgi-bin/interface.cgi?request=getReviews"
            result = urllib.request.urlopen(theRequest)
            print("reviews for " + ubit + ":")
            print(json.loads(result.read().decode('utf-8')))
            return False
    return True


def partThreeCorrect():
    passed = True
    theRequest = "http://www-student.cse.buffalo.edu/~hartloff/cgi-bin/interface.cgi?request=getAggregateRatings"
    result = urllib.request.urlopen(theRequest)
    solution = json.loads(result.read().decode('utf-8'))

    aggregate = json.loads(hw5Server.getAggregateRatings())
    if len(aggregate) != len(solution):
        passed = False
    else:
        for movie in aggregate:
            if movie not in solution.keys():
                passed = False
                break
            else:
                sol = solution[movie]
                agg = aggregate[movie]
                if not floatCompare(float(sol[0]), float(agg[0])):
                    passed = False
                    break
                if int(sol[1]) != int(agg[1]):
                    passed = False
                    break
                solReviews = sol[2]
                aggReviews = agg[2]
                if len(aggReviews) != len(solReviews):
                    passed = False
                    break
                for review in solReviews:
                    if review not in aggReviews:
                        passed = False
                        break
    if not passed:
        print("aggregate should be: " + str(solution) + "\nReturned: " + str(aggregate))
        return False
    else:
        return True


# {"title":[averageRating, numberOfRating, [textReviews]], ...}


def testPart1():
    if partOneReady():
        print(
            "Part 1 working. Push to server to host your API in the server folder with 700 permissions and submit on Planet Express.")
    else:
        print("Error in part 1")

def testPart2():
    if partTwoReady():
        print(
            "Part 2 working. Push to server to host your API in the server folder with 700 permissions and submit on Planet Express.")
    else:
        print("Error in part 2")

def testPart3():
    if partThreeCorrect():
        print("Part 3 working. Submit on Planet Express for grading.")
    else:
        print("Error in part 3")


testPart1()
testPart2()
# testPart3()
